(define-module (dot-expand util)
  #:export (path-split
	    absolute-path
	    canonicalize-path*
	    make-directory*
	    with-directory-excursion
	    directory?))

(define path-split
  (let ((cs (char-set-complement (char-set #\/))))
    (lambda (path) (string-tokenize path cs))))


(define (absolute-path path)
  (if (absolute-file-name? path)
      path
      (string-append (getcwd) "/" path)))

;; variant of canonicalize-path that doesn't check if the file exists
(define (canonicalize-path* path)
  (let loop ((acc '())
	     (components (path-split (absolute-path path))))
    (if (null? components)
	(string-join (reverse acc) "/" 'prefix)
	(let ((component (car components)))
	  (cond ((string=? component ".") (loop acc (cdr components)))
		((string=? component "..")
		 (if (null? acc)
		     (loop (cons component acc) (cdr components))
		     (loop (cdr acc) (cdr components))))
		(else (loop (cons component acc) (cdr components))))))))

;; like mkdir -p
(define make-directory*
  (lambda (path)
    (let loop ((path (if (absolute-file-name? path) "/" "." ))
	       (components (path-split path)))
      (if (null? components)
	  path
	  (let ((path (string-append path "/" (car components))))
	    (if (file-exists? path)
		(if (eq? 'directory (stat:type (lstat path)))
		    (loop path (cdr components))
		    (error "Cannot create directory file exists" path))
		(begin (mkdir path)
		       (loop path (cdr components)))))))))

(define (with-directory-excursion directory thunk)
  (let ((old-dir (getcwd)))
    (dynamic-wind (lambda () (chdir directory))
		  thunk
		  (lambda () (chdir old-dir)))))

(define (directory? path)
  (and (file-exists? path)
       (eq? 'directory (stat:type (stat path)))))
