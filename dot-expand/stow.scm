(define-module (dot-expand stow)
  #:export (stow))

(define* (stow stow-directory package target
	       #:key
	       (action 'stow) ; stow | delete
	       (ignore '())  ; list of regexs to pass to stow
	       (folding #t)
	       (simulate #f)
	       (verbose #f) ; can be #f | #t | a number from 0 to 5
	       )
  "Simple wrapper around the stow cli. Action is a symbol either stow or
delete. See stow --help and the stow info page for meaning of the other
arguments."
  (let ((args `(,@(if simulate '("--simulate") '())
		,@(if verbose
		      (if (number? verbose)
			  (list "--verbose" (number->string verbose))
			  (list "--verbose"))
		      '())
		,@(map (lambda (rx) (string-append "--ignore=" rx)) ignore)
		,@(if folding '() '("--no-folding"))
		"--dir" ,stow-directory
		"--target" ,target
		,(case action
		   ((stow) "--stow")
		   ((delete) "--delete")
		   (else (error "Invalid stow action" action)))
		,package)))
    (when verbose
      (for-each (lambda (x) (display x) (display " ")) (cons "stow" args))
      (newline))
    (let ((rc (apply system* "stow" args)))
      (or (zero? rc)
	  (error "stow exited with non-zero exit code" rc (cons "stow" args))))))
