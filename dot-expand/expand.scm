(define-module (dot-expand expand)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 ftw)
  #:use-module (srfi srfi-1)
  #:use-module (dot-expand util)
  #:export (%make-expander
	    make-expander
	    expander?
	    expander-ignore?
	    expander-ignore?-set!
	    expander-expand?
	    expander-expand?-set!
	    expander-output-directory
	    expander-output-directory-set!
	    expander-rename
	    expander-rename-set!
	    expander-pattern-regexp
	    expander-pattern-regexp-set!
	    expander-module
	    expander-module-set!
	    expander-parent-expander
	    expander-parent-expander-set!
	    
	    error-if-file-exists?
	    verbose-output-port
	    current-expander
	    input-directory
	    package-directory
	    current-file
	    
	    expand
	    expand-directory
	    expand-regular-file
	    expand-string))

;; ======================================================================
;; expander type

(define-record-type <expander>
  (%make-expander ignore?
		  expand?
		  output-directory
		  rename
		  pattern-regexp
		  module
		  parent-expander
		  post-expand-hook)
  expander?
  ;; predicate function; returns true when the file should be ignored
  (ignore? expander-ignore? expander-ignore?-set!)
  ;; predicate function; returns true when the file should be expanded
  (expand? expander-expand? expander-expand?-set!)
  ;; all input files are mapped to output files relative to this directory; this
  ;; directory is itself relative to the package-directory
  (output-directory expander-output-directory expander-output-directory-set!)
  ;; a procedure that takes an input filename and produces the output filename
  ;; (relative to the output-directory)
  (rename expander-rename expander-rename-set!)
  ;; the pattern to search for in the input file the pattern must contain a
  ;; capture group named 'expr' this is the expression which will be evaluated,
  ;; and its result substituted into the output file
  (pattern-regexp expander-pattern-regexp expander-pattern-regexp-set!)
  ;; The module in which the expressions are evaluated
  (module expander-module expander-module-set!)
  ;; the parent of the this expander
  (parent-expander expander-parent-expander expander-parent-expander-set!)
  ;; hook to run after expansion
  (post-expand-hook expander-post-expand-hook expander-post-expand-hook-set!))

(define* (make-expander #:key
			(ignore? (expander-ignore? (current-expander)))
			(expand? (expander-expand? (current-expander)))
			(output-directory (string-append (expander-output-directory (current-expander))
							 "/"
							 (dirname (current-file))))
			(rename (expander-rename (current-expander)))
			(pattern-regexp (expander-pattern-regexp (current-expander)))
			(module (expander-module (current-expander)))
			(parent-expander (current-expander))
			(post-expand #f))
  ;; TODO type checking so we can error when loading the user's file instead of
  ;; when we try to use the bad expander
  (let ((post-expand-hook (make-hook 0)))
    (when post-expand (add-hook! post-expand-hook post-expand))
    (%make-expander ignore?
		    expand?
		    output-directory
		    rename
		    pattern-regexp
		    module
		    parent-expander
		    post-expand-hook)))

;; ======================================================================
;; parameters

;; parameterize these to control behavior of expand-*
(define error-if-file-exists? (make-parameter #t))
(define verbose-output-port (make-parameter #f))  ; can be #f

;; used internally these don't need to be parameterized if your just calling
;; expand
(define current-expander (make-parameter #f))
(define current-file (make-parameter #f))
(define input-directory (make-parameter #f))
(define package-directory (make-parameter #f))

;; ======================================================================
;; verbose output

(define (verbose format-string . args)
  (if (port? (verbose-output-port))
    (apply format (verbose-output-port) format-string args)
    #f))

;; ======================================================================
;; expansion

(define (expand-regular-file input-file expander)
  (let ((output-file (string-append (package-directory)
				    "/"
				    (expander-output-directory expander)
				    "/"
				    ((expander-rename expander) input-file))))
    (make-directory* (dirname output-file))
    (cond
     ((and (file-exists? output-file) (error-if-file-exists?))
      (error "output file already exists" (canonicalize-path* output-file)))
     (((expander-expand? expander) input-file)
      (parameterize ((current-file input-file))
	(verbose "expanding~%in: ~s~%out: ~s~%~%"
		 input-file (canonicalize-path* output-file))
	(call-with-output-file output-file
	  (lambda (out)
	    (with-exception-handler
		(lambda (e)
		  (format (current-error-port) "While expanding file: ~s~%" input-file)
		  (raise-exception e))
	      (lambda ()
		(put-string out (expand-string (call-with-input-file input-file get-string-all)
					       expander)))
	      #:unwind? #t)))
	;; preserve access permissions
	;; TODO expand-file already calls stat; reuse that stat object?
	(chmod output-file (stat:perms (stat input-file)))))
     (else
      (verbose "copying~%in: ~s~%out: ~s~%~%"
	       input-file (canonicalize-path* output-file))
      (copy-file input-file output-file)))
    output-file))

(define (expand-string str expander)

  (define (substitute m)
    (let ((expr (with-input-from-string (match:substring m 1) read)))
      (eval expr (expander-module expander))))

  (regexp-substitute/global #f
			    (expander-pattern-regexp expander)
			    str
			    'pre substitute 'post))

;; invariant: config-file is relative to the current-directory
(define (call-with-config-file config-file expander proc)

  (define (make-child-expander config-file parent-expander)
    (parameterize ((current-file config-file))
      (let ((maybe-expander (eval `(primitive-load ,config-file) (expander-module expander))))
	(if (expander? maybe-expander)
	    maybe-expander
	    (%make-expander (expander-ignore? parent-expander)
			    (expander-expand? parent-expander)
			    (string-append (expander-output-directory parent-expander)
					   "/"
					   (dirname config-file))
			    (expander-rename parent-expander)
			    (expander-pattern-regexp parent-expander)
			    (expander-module parent-expander)
			    parent-expander
			    (make-hook 0))))))

  (let ((child-expander (make-child-expander config-file expander)))
    (parameterize ((current-expander child-expander))
      (with-directory-excursion (dirname config-file) (lambda () (proc child-expander))))))

(define (expand-directory directory expander)

  (define (expand-file file expander)
    (let* ((st (lstat file))
	   (type (stat:type st)))
      (if ((expander-ignore? expander) file)
	  (verbose "ignoring: ~s~%~%" file)
	  (case type
	    ((regular) (expand-regular-file file expander))
	    ((directory) (expand-directory file expander))
	    (else (error "expand-file: file is not regular or directory" file))))))

  (define (expand-files files expander)
    (for-each (lambda (f) (expand-file f expander)) files))

  (define (directory-list directory expander)

    (define (select? f) (not (or (string=? f "..") (string=? f "."))))
    
    (let ((files (scandir directory select?)))
      (if (string=? "." directory)
	  files
	  (map (lambda (f) (string-append directory "/" f)) files))))

  (let* ((files (directory-list directory expander))
	 (config-file (find (lambda (file) (string=? (basename file) ".expand.scm")) files)))
    (if config-file
	(begin
	  (verbose "loading: ~s~%~%" (canonicalize-path config-file))
	  (call-with-config-file config-file expander
	    (lambda (expander)
	      (expand-files (map basename (remove (lambda (f) (eq? f config-file)) files)) expander)
	      (run-hook (expander-post-expand-hook expander)))))
	(expand-files files expander))))


(define expand
  (let ((input-directory-parameter input-directory)
	(package-directory-parameter package-directory)
	(verbose-output-port-parameter verbose-output-port)
	(error-if-file-exists?-parameter error-if-file-exists?))
    (lambda* (input-directory
	      package-directory
	      expander
	      #:key
	      (verbose-output-port #f)
	      (error-if-file-exists? #t))
      (parameterize ((current-expander expander)
		     (input-directory-parameter (absolute-path input-directory))
		     (package-directory-parameter (absolute-path package-directory))
		     (verbose-output-port-parameter verbose-output-port)
		     (error-if-file-exists?-parameter error-if-file-exists?))
	(with-directory-excursion input-directory
	  (lambda () (expand-directory "." expander)))))))

;; ======================================================================
;; TESTING

#;
(define default-expander
  (%make-expander
   ;; don't ignore any files
   (lambda (file) #f)
   ;; expand all files
   (lambda (file) #t)
   ;; output-directory is the root of the package-directory
   "/"
   ;; don't rename files
   (lambda (file) file)
   ;; match "@@expr@@"
   (make-regexp "@@([^@]+)@@")
   (current-module)
   #f))
