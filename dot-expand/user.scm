(define-module (dot-expand user)
  #:use-module (ice-9 regex)
  #:use-module (dot-expand expand)
  #:export (default-expander
	     target-directory
	     pre-install-hook
	     post-install-hook
	     pre-uninstall-hook
	     post-uninstall-hook)
  #:re-export (make-expander
	       current-expander
	       current-file
	       input-directory
	       package-directory)
  #:declarative? #f)

;; This is the module in which the users '.expand.scm' files are loaded

(define default-expander
  (%make-expander
   ;; don't ignore any files
   (lambda (file) #f)
   ;; expand all files
   (lambda (file) #t)
   ;; output-directory is the root of the package-directory
   "/"
   ;; don't rename files
   (lambda (file) file)
   ;; match "@@expr@@" (but expr cannot contain an '@')
   (make-regexp "@@([^@]+)@@")
   ;; load user files and evaluate expressions in this module
   (current-module)
   ;; no parent expander
   #f
   ;; empty post-expand-hook
   (make-hook 0)))


(define target-directory (make-parameter #f))

(define pre-install-hook (make-hook 0))
(define post-install-hook (make-hook 0))
(define pre-uninstall-hook (make-hook 0))
(define post-uninstall-hook (make-hook 0))
