* TODO Syntax sugar

Proposal:

#+begin_src scheme
  (expander

   (ignore? "literal"            ;; exact match
            (regexp "foo$")      ;; regular expression match
            (prefix ".git")      ;; if filename has prefix
            (suffix ".in")       ;; if filename has suffix
            (=> pred)            ;; if predicate function returns #t
            ...)                 ;; also support and, or, not for composing

   (expand? ...)                 ;; same options as ignore

   (output-directory "path")     ;; filepath relative to package directory

   (rename ("input" "output")
           (strip-prefix "prefix")
           (strip-suffix "suffix")
           (add-prefix "prefix")
           (add-suffix "suffix")
           (=> proc)
           ...)                  

   (pattern-regexp regexp)       ;; takes precedence over pattern-prefix/suffix
   (pattern-prefix "@@")         ;; construct regexp for the user based on prefix and suffix
   (pattern-suffix "@@")
   (module module)               
   )
#+end_src

* TODO Make sure the input directory exists and is a directory before calling expand

Current error message is fairly cryptic.

* TODO Add a dry run option

Useful for making sure everything works. This should be easy to do, currently
only 'expand-regular-file' actually creates any files. We could even still
perform the expansions with 'expand-string' to make sure everything evaluates
correctly just don't use the result. (This would be bad if the user has embedded
code with side effects).

* TODO finish the readme

* TODO examples and/or tests

* TODO Option to preserve modification time of the input files

This would be particularly useful if we had an 'update' feature. We could use
modification time to see whether or not the output file needs to be rebuilt.

* TODO hooks

Post-expand hook , pre-install hook, post-install hook, pre-uninstall, post-uninstall

* TODO fuller interface to stow

Make a (dot-expand stow) module that provides a wrapper around the stow cli and
offer more of the stow options from dot-expand's cli. In particular I personally
would want --no-folding.

* TODO Actual management commands

It would have at least these sub-commands

- expand :: just create the stow package
- install :: link the stow package into the target directory
- uninstall :: unlink the stow package and delete it (option to only unlink?)
- update :: run uninstall -> expand -> install

I've already started to write shell script that wraps around dot-expand because
dot-expand by itself is clearly insufficient as a dotfile "manager".

Alternatively I could restrict the scope of dot-expand to only creating a stow
package. But I will end up writing more code that wraps around dot-expand for my
personal use. Might as well just add that code to dot-expand proper.

