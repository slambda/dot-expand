PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
GUILE ?= guile
GUILD ?= guild
GUILE_EFFECTIVE_VERSION = $(shell $(GUILE) -c "(display (effective-version))")
GUILE_SITE_DIR ?= $(PREFIX)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
GUILE_SITE_CCACHE_DIR ?= $(PREFIX)/lib/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

SCHEME_FILES = \
dot-expand/expand.scm \
dot-expand/util.scm \
dot-expand/user.scm \
dot-expand/stow.scm

GO_FILES = $(patsubst %.scm,build/%.go,$(SCHEME_FILES))

.PHONY = all build install clean

all: build

build: $(GO_FILES)

build/%.go: %.scm
	GUILE_LOAD_PATH="." GUILE_AUTO_COMPILE="0" $(GUILD) compile -o $@ $<

install: build bin/dot-expand
	mkdir -p $(DESTDIR)$(GUILE_SITE_DIR)
	mkdir -p $(DESTDIR)$(GUILE_SITE_CCACHE_DIR)
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -ar build/dot-expand $(DESTDIR)$(GUILE_SITE_CCACHE_DIR)/
	cp -ar dot-expand $(DESTDIR)$(GUILE_SITE_DIR)/
	cp -a bin/dot-expand $(DESTDIR)$(BINDIR)/

clean:
	rm -rf build

