;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((scheme-mode . ((geiser-scheme-implementation . guile)
		 (eval . (put 'with-directory-excursion 'scheme-indent-function 1))
		 (eval . (put 'call-with-config-file 'scheme-indent-function 2)))))
